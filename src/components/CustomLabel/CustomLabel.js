import classNames from "classnames";
import "./CustomLabel.scss";

export default function CustomLabel({ className, tag: Tag = "h3", children }) {
  return (
    <Tag className={classNames("custom-label", className)}>{children}</Tag>
  );
}
