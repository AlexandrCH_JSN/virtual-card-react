import Picture from "../../Picture/Picture";
import classNames from "classnames";
import "./CardLogo.scss";
import { useSelector } from "react-redux";
import { selectCardLogo } from "../../../redux/selectors";

export default function CardLogo({ className }) {
  const logo = useSelector(selectCardLogo);

  return (
    <div className={classNames("card-logo", className)}>
      <Picture {...logo} />
    </div>
  );
}
