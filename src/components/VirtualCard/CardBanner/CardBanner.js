import "./CardBanner.scss";
import Headline from "../../Headline/Headline";
import classNames from "classnames";
import { useSelector } from "react-redux";
import { selectCardBanner } from "../../../redux/selectors";

export default function CardBanner({ className }) {
  const { backgroundColor, color, text } = useSelector(selectCardBanner);
  const componentStyle = { backgroundColor, color };
  return (
    <div
      className={classNames("card-banner", className)}
      style={componentStyle}
    >
      <Headline tag="h2" level="1" className="card-banner__text">
        {text}
      </Headline>
    </div>
  );
}
