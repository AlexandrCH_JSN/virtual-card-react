import Headline from "../../Headline/Headline";
import Picture from "../../Picture/Picture";
import CustomLabel from "../../CustomLabel/CustomLabel";
import "./EmployeeDetails.scss";
import classNames from "classnames";
import { useSelector } from "react-redux";
import { selectEmployeeDetails } from "../../../redux/selectors";

export default function EmployeeDetails({ className }) {
  const { color, employee, validateUntil, picture, hologram } = useSelector(
    selectEmployeeDetails
  );

  const { name, position, licenseNo } = employee;

  const detailsStyle = {
    color,
  };

  const validUntilStyle = {
    color: validateUntil.color,
  };

  return (
    <div className={classNames("employee-details", className)}>
      <ul className="employee-details__list" style={detailsStyle}>
        <li className="employee-details__item">
          <CustomLabel className="employee-details__label">Name</CustomLabel>
          <Headline tag="div" level="2">
            {name}
          </Headline>
        </li>
        <li className="employee-details__item">
          <CustomLabel className="employee-details__label">
            Position
          </CustomLabel>
          <Headline tag="div" level="3">
            {position}
          </Headline>
        </li>
        {licenseNo && (
          <li className="employee-details__item">
            <CustomLabel className="employee-details__label">
              License No.
            </CustomLabel>
            <Headline tag="div" level="3">
              {licenseNo}
            </Headline>
          </li>
        )}
      </ul>
      <div className="employee-details__footer">
        <div className="employee-details__left-col">
          {hologram && (
            <div className="employee-details__hologram">
              <Picture {...hologram} />
            </div>
          )}
          <div
            className="employee-details__valid-until"
            style={validUntilStyle}
          >
            <CustomLabel>Valid until</CustomLabel>
            <Headline tag="div" level="2" style={validUntilStyle}>
              {validateUntil?.content}
            </Headline>
          </div>
        </div>
        <div className="employee-details__picture">
          {picture && <Picture {...picture} />}
        </div>
      </div>
    </div>
  );
}
