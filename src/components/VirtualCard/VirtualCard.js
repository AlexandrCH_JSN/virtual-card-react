import Preloader from "../Preloader/Preloader";
import CardLogo from "./CardLogo/CardLogo";
import CardBanner from "./CardBanner/CardBanner";
import EmployeeDetails from "./EmployeeDetails/EmployeeDetails";
import { useDispatch, useSelector } from "react-redux";
import { fetchCardData } from "../../redux/actions";
import { useEffect } from "react";
import "./VirtualCard.scss";
import { selectCard, selectLoadingStatus } from "../../redux/selectors";

function VirtualCard() {
  const dispatch = useDispatch();
  const { loading } = useSelector(selectLoadingStatus);
  const { backgroundColor } = useSelector(selectCard);

  useEffect(() => {
    dispatch(fetchCardData());
  }, [dispatch]);

  const containerStyle = {
    backgroundColor,
  };

  if (loading) {
    return (
      <div className="virtual-card" style={containerStyle}>
        <Preloader className="virtual-card__preloader" />
      </div>
    );
  }

  return (
    <div className="virtual-card" style={containerStyle}>
      <CardLogo className="virtual-card__logo" />
      <CardBanner className="virtual-card__banner" />
      <EmployeeDetails className="virtual-card__employee-details" />
    </div>
  );
}

export default VirtualCard;
