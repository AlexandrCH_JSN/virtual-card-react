import { render, screen } from "@testing-library/react";
import Picture from "./Picture";

const props = {
  src: "foo",
  srcset: "",
  width: 100,
  height: 100,
  alt: "picture",
};

const srcsetArr = [
  {
    url: "foo",
    type: "image/webp",
  },
  {
    url: "bar",
    type: "image/webp",
  },
];

describe("renders picture component", () => {
  test("renders single source", () => {
    render(<Picture {...props} />);
    const image = screen.getByAltText(props.alt);
    expect(image.src).toContain(props.src);
  });

  test("renders multiple source", () => {
    const modifiedProps = { ...props, ...{ srcset: srcsetArr } };
    render(<Picture {...modifiedProps} />);
    expect(screen.getAllByTestId("source-element")).toHaveLength(
      modifiedProps.srcset.length
    );
  });
});
