import { Fragment } from "react";

export default function Picture({
  src,
  srcset,
  width,
  height,
  alt,
  loading,
  className,
}) {
  const isImageSet = Array.isArray(srcset);

  return (
    <Fragment>
      {isImageSet ? (
        <picture>
          {srcset.map((item) => (
            <source
              data-testid="source-element"
              srcSet={item.url}
              type={item.type}
              key={item.url}
            />
          ))}
          <img
            className={className}
            src={src}
            width={width}
            height={height}
            alt={alt}
            loading={loading}
          />
        </picture>
      ) : (
        <img
          className={className}
          src={src}
          srcSet={srcset}
          width={width}
          height={height}
          alt={alt}
          loading={loading}
        />
      )}
    </Fragment>
  );
}
