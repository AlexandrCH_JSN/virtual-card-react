import { useSelector, useDispatch } from "react-redux";
import Headline from "../Headline/Headline";
import classNames from "classnames";
import TextInput from "components/Forms/TextInput/TextInput";
import { updateCardData, uploadCardLogo } from "redux/actions.js";
import "./CardControl.scss";

function CardControl({ className }) {
  const dispatch = useDispatch();
  const card = useSelector((state) => state.card);

  const handleStateChange = (newData) => {
    const payload = { ...card, ...newData };
    dispatch(updateCardData(payload));
  };

  return (
    <form className={classNames("card-control", className)}>
      <Headline tag="h2" level="3" className="card-control__title">
        Change card props
      </Headline>
      <fieldset className="card-control__fieldset">
        <legend>Common card settings</legend>
        <TextInput
          className="card-control__from-control"
          id="cardBackgroundColor"
          label="Card background color"
          type="color"
          value={card.backgroundColor}
          onChange={(event) =>
            handleStateChange({
              backgroundColor: event.target.value,
            })
          }
        />
      </fieldset>

      <fieldset className="card-control__fieldset">
        <legend>Card logo settings</legend>

        <TextInput
          className="card-control__from-control"
          id="cardLogo"
          label="Logo"
          type="file"
          onChange={(event) =>
            dispatch(
              uploadCardLogo({
                state: card,
                file: event.target.files[0],
              })
            )
          }
        />
      </fieldset>
      <fieldset className="card-control__fieldset">
        <legend>Card banner settings</legend>
        <TextInput
          className="card-control__from-control"
          id="cardBannerText"
          label="Banner text"
          value={card.banner.text}
          onChange={(event) =>
            handleStateChange({
              banner: {
                ...card.banner,
                text: event.target.value,
              },
            })
          }
        />
        <TextInput
          className="card-control__from-control"
          id="cardBannerBg"
          label="Banner background color"
          type="color"
          value={card.banner.backgroundColor}
          onChange={(event) =>
            handleStateChange({
              banner: {
                ...card.banner,
                backgroundColor: event.target.value,
              },
            })
          }
        />
        <TextInput
          className="card-control__from-control"
          id="cardBannerColor"
          label="Banner text color"
          type="color"
          value={card.banner.color}
          onChange={(event) =>
            handleStateChange({
              banner: {
                ...card.banner,
                color: event.target.value,
              },
            })
          }
        />
      </fieldset>
    </form>
  );
}

export default CardControl;
