import classNames from "classnames";
import "./TextInput.scss";

export default function TextInput({
  id,
  label,
  type = "text",
  name,
  value,
  required,
  className,
  onChange,
}) {
  return (
    <div className={classNames("text-input", `text-input--${type}`, className)}>
      {label && id && (
        <label className="text-input__label" htmlFor={id}>
          {label}
        </label>
      )}
      <input
        className={classNames(
          "text-input__field",
          `text-input__field--${type}`
        )}
        {...{ id, label, type, name, value, required }}
        onChange={(evt) => onChange(evt.nativeEvent)}
      />
    </div>
  );
}
