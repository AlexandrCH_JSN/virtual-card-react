import classNames from "classnames";
import "./Preloader.scss";

export default function Preloader({ className }) {
  return (
    <div className={classNames("preloader", className)}>
      <div className="spinner"></div>
    </div>
  );
}
