import "./Headline.scss";
import classNames from "classnames";

export default function Headline({
  className,
  tag: Tag = "h1",
  level = 1,
  children,
}) {
  return (
    <Tag
      className={classNames("headline", `headline--level-${level}`, className)}
    >
      {children}
    </Tag>
  );
}
