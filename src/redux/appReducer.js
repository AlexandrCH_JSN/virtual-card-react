import { APP_TYPES } from "./types";

const initialState = {
  loading: false,
};

export const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case APP_TYPES.SHOW_LOADER:
      return { ...state, loading: true };
    case APP_TYPES.HIDE_LOADER:
      return { ...state, loading: false };
    default:
      return state;
  }
};
