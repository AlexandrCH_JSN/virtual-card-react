import { CARD_TYPES } from "./types";

const initialState = {
  backgroundColor: "#ffffff",
  logo: {
    src: "",
    width: "",
    height: "",
    alt: "",
  },
  banner: {
    text: "",
    backgroundColor: "",
    color: "",
  },
  details: {
    color: "",
    employee: {
      name: "",
      position: "",
      licenseNo: "",
    },
    validateUntil: {
      color: "",
      content: "",
    },
    picture: {
      src: "",
      width: "",
      height: "",
      alt: "",
    },
    hologram: {
      src: "",
      width: "",
      height: "",
      alt: "",
    },
  },
};

export const cardReducer = (state = initialState, action) => {
  switch (action.type) {
    case CARD_TYPES.GET_CARD_DATA:
      return { ...state, ...action.payload };
    case CARD_TYPES.UPDATE_CARD_DATA:
      return { ...state, ...action.payload };
    case CARD_TYPES.UPLOAD_CARD_LOGO:
      return {
        ...state,
        logo: action.payload,
      };
    default:
      return state;
  }
};
