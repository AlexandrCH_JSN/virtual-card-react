export const selectCard = (state) => state.card;

export const selectCardBanner = (state) => state.card.banner;

export const selectCardLogo = (state) => state.card.logo;

export const selectEmployeeDetails = (store) => store.card.details;

export const selectLoadingStatus = (state) => state.app;
