import { takeEvery, put, call } from "redux-saga/effects";
import { CARD_TYPES } from "./types";
import { showLoader, hideLoader } from "./actions";
import Api from "../services/Api";

export function* sagaWatcher() {
  yield takeEvery(CARD_TYPES.UPLOAD_CARD_LOGO, uploadCardLogo);
  yield takeEvery(CARD_TYPES.REQUEST_CARD_DATA, requestCardData);
}

function* requestCardData() {
  try {
    yield put(showLoader());
    const payload = yield call(Api.fetchCard);
    yield put({
      type: CARD_TYPES.GET_CARD_DATA,
      payload,
    });
    yield put(hideLoader());
  } catch (e) {}
}

function* uploadCardLogo({ payload }) {
  try {
    yield put(showLoader());
    const { state, file } = payload;
    const imageData = yield call(() => Api.uploadImage(file));
    yield put({
      type: CARD_TYPES.UPDATE_CARD_DATA,
      payload: {
        ...state,
        logo: imageData,
      },
    });
    yield put(hideLoader());
  } catch (e) {}
}
