import { APP_TYPES, CARD_TYPES } from "./types";

export function showLoader() {
  return {
    type: APP_TYPES.SHOW_LOADER,
  };
}

export function hideLoader() {
  return {
    type: APP_TYPES.HIDE_LOADER,
  };
}

export function fetchCardData() {
  return {
    type: CARD_TYPES.REQUEST_CARD_DATA,
  };
}

export function updateCardData(payload) {
  return {
    type: CARD_TYPES.UPDATE_CARD_DATA,
    payload,
  };
}

export function uploadCardLogo(payload) {
  return {
    type: CARD_TYPES.UPLOAD_CARD_LOGO,
    payload,
  };
}
