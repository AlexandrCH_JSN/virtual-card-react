import { getImageSize } from "helperFunctions/image";

const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

const isDevMode = () => process.env.NODE_ENV === "development";

const Api = {
  async fetchCard() {
    const response = await fetch(process.env.REACT_APP_CARD_DATA_URL);

    if (isDevMode()) {
      await sleep(process.env.REACT_APP_FETCH_DELAY);
    }

    const cardData = await response.json();

    return {
      backgroundColor: cardData.cardBackground,
      logo: {
        src: cardData.logoSrc,
        width: cardData.logoWidth,
        height: cardData.logoHeight,
        alt: cardData.logoAlt,
      },
      banner: {
        text: cardData.bannerText,
        backgroundColor: cardData.bannerBackgroundColor,
        color: cardData.bannerTextColor,
      },
      details: {
        color: cardData.detailsTextColor,
        employee: {
          name: cardData.employeeName,
          position: cardData.employeePosition,
          licenseNo: cardData.employeeLicenseNo,
        },
        validateUntil: {
          color: cardData.validUntilColor,
          content: cardData.validuntilContent,
        },
        picture: {
          src: cardData.pictureSrc,
          width: cardData.pictureWidth,
          height: cardData.pictureHeight,
          alt: cardData.pictureAlt,
        },
        hologram: {
          src: cardData.hologramUrl,
          width: cardData.hologramWidth,
          height: cardData.hologramHeight,
          alt: cardData.hologramAlt,
        },
      },
    };
  },
  async uploadImage(file) {
    return isDevMode
      ? new Promise((resolve) => {
          let reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = async () => {
            const src = reader.result;
            const { width, height } = getImageSize(src);
            const alt = "Uploaded image";
            await sleep(process.env.REACT_APP_FETCH_DELAY);
            resolve({
              src,
              width,
              height,
              alt,
            });
          };
        })
      : new Promise(async (resolve) => {
          const imageData = await fetch(process.env.REACT_APP_UPLOAD_IMAGE, {
            method: "POST",
            body: file,
          });

          resolve(imageData);
        });
  },
};

export default Api;
