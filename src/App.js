import "./App.css";
import CardControl from "./components/CardControl/CardControl";
import Headline from "./components/Headline/Headline";
import VirtualCard from "./components/VirtualCard/VirtualCard";

function App() {
  return (
    <div className="App">
      <div className="App__title">
        <Headline tag="h1">Virtual Card</Headline>
      </div>
      <div className="App__content">
        <CardControl className="App__card-control" />
        <VirtualCard />
      </div>
    </div>
  );
}

export default App;
